//
//  main.m
//  KTF
//
//  Created by Justin Oakes on 9/11/15.
//  Copyright © 2015 Assure Vote. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
