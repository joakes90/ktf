//
//  AppearanceController.m
//  KTF
//
//  Created by Justin Oakes on 9/11/15.
//  Copyright © 2015 Assure Vote. All rights reserved.
//

#import "AppearanceController.h"
#import <UIKit/UIKit.h>
@implementation AppearanceController

+ (void) setAppearanceAttributes {
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor blackColor],
                                               NSForegroundColorAttributeName,
                                               [UIFont fontWithName:@"EraserDust" size:18.0],
                                               NSFontAttributeName,
                                               nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    [[UINavigationBar appearance] setBackgroundColor:[UIColor blackColor]];
    [[UINavigationBar   appearance] setTintColor:[UIColor colorWithRed:196.0/255.0 green:24.0/255.0 blue:0 alpha:1]];
}

@end
