//
//  AppearanceController.h
//  KTF
//
//  Created by Justin Oakes on 9/11/15.
//  Copyright © 2015 Assure Vote. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppearanceController : NSObject

+ (void) setAppearanceAttributes;

@end
