//
//  ViewController.m
//  KTF
//
//  Created by Justin Oakes on 9/11/15.
//  Copyright © 2015 Assure Vote. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

//text fields
@property (strong, nonatomic) IBOutlet UITextField *studentCostTextField;
@property (strong, nonatomic) IBOutlet UITextField *classroomDollarsTextField;
@property (strong, nonatomic) IBOutlet UITextField *nonClassroomTextField;

//labels
@property (strong, nonatomic) IBOutlet UILabel *classTotalLabel;
@property (strong, nonatomic) IBOutlet UILabel *facilitiesLabel;
@property (strong, nonatomic) IBOutlet UILabel *foodLabel;
@property (strong, nonatomic) IBOutlet UILabel *adminLabel;
@property (strong, nonatomic) IBOutlet UILabel *studentSupportLabel;
@property (strong, nonatomic) IBOutlet UILabel *instructionSupportLabel;
@property (strong, nonatomic) IBOutlet UILabel *transportLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalLabel;
@property (strong, nonatomic) IBOutlet UILabel *proposalLabel;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;

@property (strong, nonatomic) IBOutlet UIButton *buttonOutlet;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.studentCostTextField.delegate = self;
    self.classroomDollarsTextField.delegate = self;
    self.nonClassroomTextField.delegate = self;
}

-(void)viewDidAppear:(BOOL)animated {
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.scrollView.contentSize.height);
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(screenTapped)];
    singleTap.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:singleTap];
}

- (IBAction)unhideProposal:(id)sender {
    if ([self.studentCostTextField.text isEqualToString:@"$"] || [self.classroomDollarsTextField.text isEqualToString:@"-$"] || [self.nonClassroomTextField.text isEqualToString:@"-$"]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Fill out all fields"
                                                                                 message:@"Please fill out the cost per student, in classroom, and non classroom spending text fields to see Governor Ducey’s budget proposal"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        self.classroomDollarsTextField.text = @"$81,338";
        self.classTotalLabel.text = @"$146,531";
        self.proposalLabel.hidden = NO;
        self.buttonOutlet.enabled = NO;
        self.messageLabel.text = @"SUPPORT GOVERNOR DUCEY'S K-12 EDUCATION PLAN";
    }
}

- (IBAction)clearAll:(id)sender {
    self.studentCostTextField.text = @"$";
    self.studentCostTextField.enabled = YES;
    self.classroomDollarsTextField.text = @"-$";
    self.classroomDollarsTextField.enabled = YES;
    self.classTotalLabel.text = @"$0.00";
    self.nonClassroomTextField.text = @"-$";
    self.nonClassroomTextField.enabled = YES;
    self.facilitiesLabel.text = @"$0.00";
    self.foodLabel.text = @"$0.00";
    self.adminLabel.text = @"$0.00";
    self.studentSupportLabel.text = @"$0.00";
    self.instructionSupportLabel.text = @"$0.00";
    self.transportLabel.text = @"$0.00";
    self.messageLabel.text = @"SEE GOV. DUCEY'S IN CLASSROOM BUDGET PROPOSAL";
    self.totalLabel.text = @"0.00";
    self.proposalLabel.hidden = YES;
    self.buttonOutlet.enabled = YES;
}

- (void)screenTapped {
    NSDictionary *correctValues = [NSDictionary dictionaryWithObjects:@[[NSNumber numberWithInt:7578],
                                                                        [NSNumber numberWithInt:75758],
                                                                        [NSNumber numberWithInt:65193]]
                                                              forKeys:@[@"studentCost",
                                                                        @"classCost",
                                                                        @"nonClassCost"]];
    NSArray *textFields = @[self.studentCostTextField, self.classroomDollarsTextField, self.nonClassroomTextField];
    
    for (UITextField *textField in textFields) {
        if ([textField isFirstResponder]) {
            [textField resignFirstResponder];
            NSString *cleanedNumberString = [textField.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
            cleanedNumberString = [cleanedNumberString stringByReplacingOccurrencesOfString:@"-" withString:@""];
            NSInteger textFieldIntValue = [cleanedNumberString integerValue];
           
            //adding the correct values to the screen
            if ([textField.restorationIdentifier isEqualToString:@"studentCost"]) {
                    textField.text = @"$7,578";
                    self.classTotalLabel.text = @"$140,951";
                    self.totalLabel.text = [NSString stringWithFormat:@"$%ld", (long)[self totalValueAdd:[NSNumber numberWithInteger:140951] subtract:nil]];
                        textField.enabled = NO;
                
            } else if ([textField.restorationIdentifier isEqualToString:@"classCost"]) {
                    textField.text = @"- $75,758";
                    self.totalLabel.text = [NSString stringWithFormat:@"$%ld", (long)[self totalValueAdd:nil subtract:[NSNumber numberWithInteger:75758]]];
                        textField.enabled = NO;
            } else {
                    textField.text = @"$65,193";
                    self.facilitiesLabel.text = @"- $17,168";
                    self.foodLabel.text = @"- $7,533";
                    self.adminLabel.text = @"- $14,080";
                    self.studentSupportLabel.text = @"- $11,160";
                    self.instructionSupportLabel.text = @"- $8.314";
                    self.transportLabel.text = @"- $6,938";
                    self.totalLabel.text = [NSString stringWithFormat:@"$%ld", (long)[self totalValueAdd:nil subtract:[NSNumber numberWithInteger:65193]]];
                        textField.enabled = NO;
            }
            // creating alert view
            if (textFieldIntValue != [correctValues[textField.restorationIdentifier] integerValue]) {
                NSInteger thousands = (long)[correctValues[textField.restorationIdentifier] integerValue] / 1000;
                NSInteger hundreds = (long)[correctValues[textField.restorationIdentifier] integerValue] - (thousands * 1000);
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Not quite right"
                                                                                         message:[NSString stringWithFormat:@"We actually spend $%ld,%ld every year on this", (long)thousands, (long)hundreds]
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
        }
    }
}


- (NSInteger)totalValueAdd:(NSNumber *_Nullable)add subtract:(NSNumber *_Nullable)subtract {
    NSString *sanitizedTotalString = [self.totalLabel.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    NSInteger totalValue = [sanitizedTotalString intValue];
    
    if (add) {
        totalValue += [add intValue];
    }
    if (subtract) {
        totalValue -= [subtract intValue];
    }
    return totalValue;
}

//textfield delegate methods
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    BOOL hasCurrentFirstResponder = NO;
    NSArray *textFields = @[self.studentCostTextField, self.classroomDollarsTextField, self.nonClassroomTextField];
    for (UITextField *textField in textFields) {
        if ([textField isFirstResponder]) {
            hasCurrentFirstResponder = YES;
        }
    }
    if (hasCurrentFirstResponder) {
        return NO;
    } else {
        return YES;
    }
}


@end