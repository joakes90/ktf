//
//  MoreInfoViewController.m
//  KTF
//
//  Created by Justin Oakes on 9/14/15.
//  Copyright © 2015 Assure Vote. All rights reserved.
//

#import "MoreInfoViewController.h"
#import <SafariServices/SafariServices.h>

@interface MoreInfoViewController () <SFSafariViewControllerDelegate>

@end

@implementation MoreInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view. 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)openKTFWebPage:(id)sender {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        SFSafariViewController *safariView = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:@"http://kidsandteachersfirst.com"]];
        safariView.delegate = self;
        [self presentViewController:safariView animated:YES completion:nil];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://kidsandteachersfirst.com"]];
    }
}

- (IBAction)openStateSpendingWebPage:(id)sender {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        SFSafariViewController *safariView = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:@"http://www.azauditor.gov/reports-publications/school-districts/multiple-school-district/report/arizona-school-district-0"]];
        safariView.delegate = self;
        [self presentViewController:safariView animated:YES completion:nil];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.azauditor.gov/reports-publications/school-districts/multiple-school-district/report/arizona-school-district-0"]];
    }

}

- (IBAction)openProposalWebPage:(id)sender {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        SFSafariViewController *safariView = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:@"http://www.azcentral.com/story/news/arizona/politics/2015/06/04/ducey-education-plan/28487427/"]];
        safariView.delegate = self;
        [self presentViewController:safariView animated:YES completion:nil];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.azcentral.com/story/news/arizona/politics/2015/06/04/ducey-education-plan/28487427/"]];
    }

}

// Safari View controller dellegate methods

-(void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
